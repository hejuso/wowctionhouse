package com.example.hejuso.wowctionhouse.App.Model;

public class Subastas {

    private String owner;
    private String item;
    private String buyout;
    private String bid;
    private String quantity;

    public Subastas(String owner, String item, String quantity, String buyout, String bid) {
        this.owner = owner;
        this.item = item;
        this.buyout = buyout;
        this.bid = bid;
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getBuyout() {
        return buyout;
    }

    public void setBuyout(String buyout) {
        this.buyout = buyout;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }
}
