package com.example.hejuso.wowctionhouse.App.Controller;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hejuso.wowctionhouse.App.Adapter.SeccionesAdapter;
import com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarObjeto;
import com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarUsuario;
import com.example.hejuso.wowctionhouse.R;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    FirebaseAuth mAuth;
    SeccionesAdapter mSeccionesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences pref = getSharedPreferences("Preferencias", MODE_PRIVATE);

        mSeccionesAdapter = new SeccionesAdapter(getSupportFragmentManager());

        ViewPager mPaginadorVista = findViewById(R.id.container);
        setupViewPager(mPaginadorVista);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPaginadorVista);

        // Navigation Drawer
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        mAuth = FirebaseAuth.getInstance();
        if (mToggle.onOptionsItemSelected(item)) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        SeccionesAdapter adapter = new SeccionesAdapter(getSupportFragmentManager());

        adapter.addFragment(new FragmentBuscarUsuario(), "Item by username");
        adapter.addFragment(new FragmentBuscarObjeto(), "Search item");

        viewPager.setAdapter(adapter);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.drawer_menu) {
            Toast.makeText(this, "You've been logged out successfully", Toast.LENGTH_SHORT).show();
            mAuth.signOut();
            android.content.Intent loginIntent = new android.content.Intent(getApplicationContext(),
                    LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        return false;
    }
}
