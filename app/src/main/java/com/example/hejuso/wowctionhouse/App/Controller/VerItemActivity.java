package com.example.hejuso.wowctionhouse.App.Controller;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hejuso.wowctionhouse.App.Adapter.AdaptadorRecycler;
import com.example.hejuso.wowctionhouse.App.Model.BuscarPorObjetoId;
import com.example.hejuso.wowctionhouse.App.Model.Subastas;
import com.example.hejuso.wowctionhouse.R;

import java.util.ArrayList;

import static com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarUsuario.listadoSubastas;
import static com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarUsuario.mLayoutManager;
import static java.lang.Integer.parseInt;

public class VerItemActivity extends AppCompatActivity {
    public static AdaptadorRecycler mAdapter;
    public static ArrayList<Subastas> subasta;
    public static ProgressBar loadAuctions;
    public static TextView noAuctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_item);
        SharedPreferences pref = getSharedPreferences("Preferencias", MODE_PRIVATE);
        subasta = new ArrayList<Subastas>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String mItem_id = extras.getString("ITEM_ID");
            int item_id = parseInt(mItem_id);
            String realm = pref.getString("realm", "argent-dawn");
            String locale = pref.getString("locale", "es_ES");

            loadAuctions = findViewById(R.id.loadAuctionsId);
            noAuctions = findViewById(R.id.noAuctions);
            listadoSubastas = findViewById(R.id.listadoSubastasId);
            mAdapter = new AdaptadorRecycler(this, subasta);
            listadoSubastas.setAdapter(mAdapter);
            mLayoutManager = new LinearLayoutManager(this);
            listadoSubastas.setLayoutManager(mLayoutManager);

            BuscarPorObjetoId proceso = new BuscarPorObjetoId(this, realm, locale, item_id);

            loadAuctions.setVisibility(View.VISIBLE);
            loadAuctions.getLayoutParams().height = 300;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                proceso.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                proceso.execute();
            }

        }
    }
}
