package com.example.hejuso.wowctionhouse.App.Model;

import com.example.hejuso.wowctionhouse.App.Controller.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hejuso on 19/02/2018.
 */

public class FetchData extends android.os.AsyncTask<Void, Void, Void> {
    private String dataJson = "";
    private String dataJson2 = "";
    private String url_api;
    private String url_api2;
    private String realm;
    private String locale;

    public FetchData(String realm, String locale) {
        this.realm = realm;
        this.locale = locale;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Ejecución asíncrona
     */
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url_first = new URL("https://eu.api.battle.net/wow/auction/data/" + this.getRealm() + "?locale=" + this.getLocale() + "&apikey=5ttqxg762en2qy9eye8nmcuf4b9sy6cd");

            // Abrimos conexión HTTP
            HttpURLConnection httpURLConnection = (HttpURLConnection) url_first.openConnection();

            // Leemos los datos que nos proporciona la conexión y recogemos la url
            InputStream inputStream = httpURLConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while (line != null) {
                line = bufferedReader.readLine();
                dataJson = dataJson + line;
            }

            // nos guardamos la variable URL
            JSONObject objectUrlJSON = new JSONObject(dataJson);
            JSONArray urlJSON = objectUrlJSON.getJSONArray("files");
            for (int i = 0; i < urlJSON.length(); i++) {
                JSONObject data_json = urlJSON.getJSONObject(i);
                url_api = data_json.getString("url");
            }

            URL url_last = new URL(url_api);

//             Abrimos conexión HTTP
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) url_last.openConnection();
            // Leemos los datos que nos proporciona la conexión
            InputStream inputStream2 = httpURLConnection2.getInputStream();
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream2));
            String line2 = "";

            while (line2 != null) {
                line2 = bufferedReader2.readLine();
                dataJson2 = dataJson2 + line2;
            }

            // nos guardamos la variable URL
            JSONObject objectUrlJSON2 = new JSONObject(dataJson2);
            JSONArray urlJSON2 = objectUrlJSON2.getJSONArray("realms");
            for (int i = 0; i < urlJSON2.length(); i++) {
                JSONObject data_json2 = urlJSON2.getJSONObject(i);
                url_api2 = data_json2.getString("name");
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Despues de ejecutar
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

    }

}