package com.example.hejuso.wowctionhouse.App.Model;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.example.hejuso.wowctionhouse.App.Controller.VerItemActivity;
import com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarUsuario;
import com.example.hejuso.wowctionhouse.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BuscarPorObjetoId extends android.os.AsyncTask<Void, Void, Void> {
    private String realm;
    private String locale;
    private int idObjeto;
    private Context context;
    private ArrayList<Subastas> subastas;

    public BuscarPorObjetoId(Context context, String realm, String locale, int idObjeto) {
        this.realm = realm;
        this.locale = locale;
        this.idObjeto = idObjeto;
        this.context = context;
    }

    public String getRealm() {
        return realm;
    }

    public String getLocale() {
        return locale;
    }

    public int getIdObjeto() {
        return idObjeto;
    }

    private Context getContext() {
        return context;
    }

    /**
     * Ejecución asíncrona
     */
    @Override
    protected Void doInBackground(Void... voids) {

        // Encontrar subastas del usuario
        subastas = this.findUserAuction();
        return null;
    }

    /**
     * Despues de ejecutar
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Log.v("ITEMS AUC", "SANSACABO?");
        /* Recycler view */
        VerItemActivity.subasta.addAll(subastas);

        VerItemActivity.mAdapter.notifyDataSetChanged();

        if (subastas.size() == 0){
            VerItemActivity.noAuctions.setVisibility(View.VISIBLE);
        }

        VerItemActivity.loadAuctions.setVisibility(View.INVISIBLE);
        VerItemActivity.loadAuctions.getLayoutParams().height = 0;

    }

    private ArrayList<Subastas> findUserAuction() {

        ArrayList<Subastas> subasta = new ArrayList<Subastas>();
        String goldBuyout = "";
        String goldBid = "";
        String itemString = "";
        String pathAuctionsJson = "";
        pathAuctionsJson = getContext().getFilesDir() + "/auctions.json";
        int id_anterior = -1;
        String name_anterior = "";

        String urlJson = this.getJSONData(pathAuctionsJson);

        // nos guardamos la variable URL
        JSONObject objectUrlJSON = null;

        try {
            objectUrlJSON = new JSONObject(urlJson);
            JSONArray urlJSON = objectUrlJSON.getJSONArray("auctions");
            for (int i = 0; i < urlJSON.length(); i++) {
                JSONObject data_json = urlJSON.getJSONObject(i);
                if (getIdObjeto() == data_json.getInt("item")) {

                    /* ORO BUYOUT */
                    goldBuyout = parseGold(data_json.getInt("buyout"));
                    goldBid = parseGold(data_json.getInt("bid"));
                    itemString = parseIdItem(getIdObjeto());
                    subasta.add(new Subastas(data_json.getString("owner"), itemString, " x" + data_json.getString("quantity"), goldBuyout, goldBid));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return subasta;
    }


    private String getJSONData(String textFileName) {
        String strJSON;
        StringBuilder buf = new StringBuilder();
        InputStream json;
        try {
            json = new FileInputStream(textFileName);

            BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

            while ((strJSON = in.readLine()) != null) {
                buf.append(strJSON);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buf.toString();
    }

    private String parseGold(int currency) {

        int[] totalGold = new int[3];
        String totalGoldStr = "";
        int copper = currency % 100;
        currency = (currency - copper) / 100;
        int silver = currency % 100;
        int gold = (currency - silver) / 100;
        totalGold[0] = copper;
        totalGold[1] = silver;
        totalGold[2] = gold;

        totalGoldStr = totalGold[2] + "g " + totalGold[1] + "s " + totalGold[0] + "s";

        return totalGoldStr;
    }

    private String parseIdItem(int id_item) {
        String itemString = "";

        try {
            URL url_item = new URL("https://eu.api.battle.net/wow/item/" + id_item + "?locale=" + this.getLocale() + "&apikey=5ttqxg762en2qy9eye8nmcuf4b9sy6cd");
            String jsonStr = "";
            // Abrimos conexión HTTP
            HttpURLConnection httpURLConnection = (HttpURLConnection) url_item.openConnection();

            // Leemos los datos que nos proporciona la conexión y recogemos la url
            InputStream inputStream = httpURLConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while (line != null) {
                line = bufferedReader.readLine();
                jsonStr = jsonStr + line;
            }

            JSONObject json = new JSONObject(jsonStr);

            itemString = json.getString("name");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return itemString;
    }

}