package com.example.hejuso.wowctionhouse.App.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hejuso.wowctionhouse.App.Adapter.AdaptadorItemRecycler;
import com.example.hejuso.wowctionhouse.App.Controller.VerItemActivity;
import com.example.hejuso.wowctionhouse.App.Listeners.RecyclerItemClickListener;
import com.example.hejuso.wowctionhouse.App.Model.Objeto;
import com.example.hejuso.wowctionhouse.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentBuscarObjeto extends Fragment {
    private static final String TAG = "BuscarObjeto";

    private Button buscarBtn;
    private SharedPreferences pref;
    private EditText nombreBusca;
    private FirebaseAuth mAuth;
    DatabaseReference bbdd;

    /* Recycler View */
    public static RecyclerView listadoObjetos;
    public static AdaptadorItemRecycler mAdapter;
    public static RecyclerView.LayoutManager mLayoutManager;
    public static ArrayList<Objeto> objeto;
    public static ProgressBar loadItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        objeto = new ArrayList<Objeto>();

        View view = inflater.inflate(R.layout.tab2_fragment, container, false);
        pref = this.getActivity().getSharedPreferences("Preferencias", MODE_PRIVATE);

        buscarBtn = view.findViewById(R.id.buscarBtnObj);
        nombreBusca = view.findViewById(R.id.nombreBuscarObj);
        loadItems = view.findViewById(R.id.loadItems);

        listadoObjetos = view.findViewById(R.id.listadoObjetos);

        /* OBJETO DE EJEMPLO */

//        objeto.add(new Objeto("Nombre", "icon"));

        mAdapter = new AdaptadorItemRecycler(view.getContext(), objeto);
        listadoObjetos.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        listadoObjetos.setLayoutManager(mLayoutManager);

        // Buscar objetos
        buscarBtn.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(android.view.View view) {

                if (!nombreBusca.getText().toString().equals("")) {

                    String nombreString = nombreBusca.getText().toString();

                    DatabaseReference referencia = FirebaseDatabase.getInstance().getReference("items");

                    Query q = referencia.orderByChild("name")
                            .startAt(nombreString)
                            .endAt(nombreString + "\uf8ff");

                    q.addValueEventListener(valueEventListener);

                    loadItems.setVisibility(View.VISIBLE);
                    FragmentBuscarObjeto.loadItems.getLayoutParams().height = 300;

                } else {
                    Toast.makeText(view.getContext(), "Introduce un nombre", Toast.LENGTH_LONG).show();
                }

            }

            ValueEventListener valueEventListener = new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    clearData();

                    // Recorremos el array de los datos de firebase
                    for (DataSnapshot datasnapshot : dataSnapshot.getChildren()) {
                        Objeto items = datasnapshot.getValue(Objeto.class);
                        objeto.add(items);
                    }

                    /* Recycler view */
                    FragmentBuscarObjeto.mAdapter.notifyDataSetChanged();

                    FragmentBuscarObjeto.loadItems.setVisibility(View.INVISIBLE);
                    FragmentBuscarObjeto.loadItems.getLayoutParams().height = 0;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }

                public void clearData() {
                    objeto.clear(); //clear list
                    mAdapter.notifyDataSetChanged(); //let your adapter know about the changes and reload view.
                }

            };

        });

        listadoObjetos.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), listadoObjetos, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        TextView mIdHidden = view.findViewById(R.id.idHidden);
                        String idHidden = mIdHidden.getText().toString();

                        Intent intent = new Intent(getContext(), VerItemActivity.class);
                        intent.putExtra("ITEM_ID", idHidden);
                        startActivity(intent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        Log.v("TOCADO", "HE TOCADO SEGUIDO " + position);
                    }
                })
        );

        return view;
    }


}
