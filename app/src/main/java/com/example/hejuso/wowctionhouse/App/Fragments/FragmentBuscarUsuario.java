package com.example.hejuso.wowctionhouse.App.Fragments;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hejuso.wowctionhouse.App.Adapter.AdaptadorRecycler;
import com.example.hejuso.wowctionhouse.App.Model.BuscarPorUsuario;
import com.example.hejuso.wowctionhouse.App.Model.Subastas;
import com.example.hejuso.wowctionhouse.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentBuscarUsuario extends Fragment {
    private static final String TAG = "BuscarUsuario";

    private Button buscarBtn;
    private SharedPreferences pref;
    private EditText nombreBusca;

    /* Recycler View */
    public static RecyclerView listadoSubastas;
    public static AdaptadorRecycler mAdapter;
    public static RecyclerView.LayoutManager mLayoutManager;
    public static ArrayList<Subastas> subasta;
    public static ProgressBar loadAuctions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        subasta = new ArrayList<Subastas>();

        View view = inflater.inflate(R.layout.tab1_fragment, container, false);
        pref = this.getActivity().getSharedPreferences("Preferencias", MODE_PRIVATE);

        buscarBtn = view.findViewById(R.id.buscarBtn);
        nombreBusca = view.findViewById(R.id.nombreBuscar);
        loadAuctions = view.findViewById(R.id.loadAuctions);

        listadoSubastas = view.findViewById(R.id.listadoSubastas);
        mAdapter = new AdaptadorRecycler(view.getContext(), subasta);
        listadoSubastas.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(view.getContext());
        listadoSubastas.setLayoutManager(mLayoutManager);

        // Buscar subastas de usuarios
        buscarBtn.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(android.view.View view) {

                String realm = pref.getString("realm", "argent-dawn");
                String locale = pref.getString("locale", "es_ES");

                String nombreString = nombreBusca.getText().toString();

                if (!nombreBusca.getText().toString().equals("")) {
                    BuscarPorUsuario proceso = new BuscarPorUsuario(getContext(), realm, locale, nombreString);
                    loadAuctions.setVisibility(View.VISIBLE);
                    FragmentBuscarObjeto.loadItems.getLayoutParams().height = 300;
                    if (subasta != null) {
                        subasta.clear();
                        mAdapter.notifyDataSetChanged(); //let your adapter know about the changes and reload view.
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        proceso.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        proceso.execute();
                    }

                } else {
                    Toast.makeText(view.getContext(), "Introduce un nombre", Toast.LENGTH_LONG).show();
                }

            }

        });
        return view;
    }

}
