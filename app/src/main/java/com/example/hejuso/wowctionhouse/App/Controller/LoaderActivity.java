package com.example.hejuso.wowctionhouse.App.Controller;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoaderActivity extends AppCompatActivity {
    private SharedPreferences pref;

    String url_api;
    long lastModified;
    InputStream jsonInputUrl = null;
    OutputStream firstJson = null;
    OutputStream auctionsJson = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.hejuso.wowctionhouse.R.layout.activity_loader);
    }

    @Override
    protected void onStart() {
        super.onStart();
        pref = getSharedPreferences("Preferencias", MODE_PRIVATE);

        String realm = pref.getString("realm", "argent-dawn");
        String locale = pref.getString("locale", "es_ES");

        DownloadAuction proceso = new DownloadAuction(locale, realm);
        proceso.execute();

        setContentView(com.example.hejuso.wowctionhouse.R.layout.activity_loader);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class DownloadAuction extends AsyncTask<Void, Void, Boolean> {

        private String realm;
        private String locale;

        DownloadAuction(String locale, String realm) {
            this.realm = realm;
            this.locale = locale;
        }

        public String getRealm() {
            return realm;
        }

        public String getLocale() {
            return locale;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            // Descarga el JSON
            try {
                jsonInputUrl = new URL("https://eu.api.battle.net/wow/auction/data/" + getRealm() + "?" + getLocale() + "&apikey=5ttqxg762en2qy9eye8nmcuf4b9sy6cd").openStream();
                String pathJson = getFilesDir() + "/output.json";
                firstJson = new FileOutputStream(pathJson);

                /* Parseamos segundo string */
                File urlJsonFile = new File(pathJson);

                FileInputStream stream = new FileInputStream(urlJsonFile);

                byte[] buffer = new byte[1024];
                for (int length = 0; (length = jsonInputUrl.read(buffer)) > 0; ) {
                    firstJson.write(buffer, 0, length);
                }

                String urlJson = this.getJSONData(pathJson);

                // nos guardamos la variable URL
                JSONObject objectUrlJSON = null;

                objectUrlJSON = new JSONObject(urlJson);
                JSONArray urlJSON = objectUrlJSON.getJSONArray("files");
                for (int i = 0; i < urlJSON.length(); i++) {
                    JSONObject data_json = urlJSON.getJSONObject(i);
                    url_api = data_json.getString("url");
                    lastModified = data_json.getLong("lastModified");
                }

                SharedPreferences sharedPref = getSharedPreferences("Preferencias", MODE_PRIVATE);

                long fechaPreferencias = sharedPref.getLong("Ultima fecha modificada JSON", 0);
                String pathAuctionsJson = getFilesDir() + "/auctions.json";
                File file = new File(pathAuctionsJson);
                if (file.exists()) {

                    if (fechaPreferencias != 0 && fechaPreferencias < lastModified) {

                        // Guardamos la fecha actual
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putLong("Ultima fecha modificada JSON", lastModified).apply();
                        editor.putString("lasturl", url_api).apply();

                        // Descargamos el archivo
                        URL url_last = new URL(url_api);

                        InputStream urlAuction = url_last.openStream();


                        auctionsJson = new FileOutputStream(pathAuctionsJson);

                        byte[] buffer2 = new byte[1024];

                        for (int length = 0; (length = urlAuction.read(buffer2)) > 0; ) {
                            auctionsJson.write(buffer2, 0, length);
                        }

                    } else {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putLong("Ultima fecha modificada JSON", fechaPreferencias).apply();
                    }

                } else {

                    // Guardamos la fecha actual
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putLong("Ultima fecha modificada JSON", fechaPreferencias).apply();
                    editor.putString("lasturl", url_api).apply();

                    // Descargamos el archivo
                    URL url_last = new URL(url_api);

                    InputStream urlAuction = url_last.openStream();

                    pathAuctionsJson = getFilesDir() + "/auctions.json";
                    auctionsJson = new FileOutputStream(pathAuctionsJson);

                    byte[] buffer2 = new byte[1024];

                    for (int length = 0; (length = urlAuction.read(buffer2)) > 0; ) {
                        auctionsJson.write(buffer2, 0, length);
                    }

                }


                // Here you could append further stuff to `output` if necessary.
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.v("JSON", "FileNotFoundException EXCEPTION " + e);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.v("JSON", "MalformedURLException EXCEPTION " + e);
            } catch (IOException e) {
                e.printStackTrace();
                Log.v("JSON", "IO EXCEPTION " + e);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                finishAffinity();
                android.content.Intent mainIntent = new android.content.Intent(getApplicationContext(),
                        MainActivity.class);
                startActivity(mainIntent);
                finish();
            } else {
                Log.v("JSON", "Pero kapasao ");
            }
        }

        private String getJSONData(String textFileName) {
            String strJSON;
            StringBuilder buf = new StringBuilder();
            InputStream json;
            try {
                json = new FileInputStream(textFileName);

                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

                while ((strJSON = in.readLine()) != null) {
                    buf.append(strJSON);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return buf.toString();
        }

    }

}
