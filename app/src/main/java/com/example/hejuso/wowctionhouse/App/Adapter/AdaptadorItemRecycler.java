package com.example.hejuso.wowctionhouse.App.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hejuso.wowctionhouse.App.Fragments.FragmentBuscarUsuario;
import com.example.hejuso.wowctionhouse.App.Model.Objeto;
import com.example.hejuso.wowctionhouse.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class AdaptadorItemRecycler extends RecyclerView.Adapter {
    private ArrayList<Objeto> mData;
    private LayoutInflater mInflater;

    public AdaptadorItemRecycler(Context context, ArrayList<Objeto> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    public ArrayList<Objeto> getmData() {
        return mData;
    }

    public void setmData(ArrayList<Objeto> mData) {
        this.mData = mData;
    }

    public LayoutInflater getmInflater() {
        return mInflater;
    }

    public void setmInflater(LayoutInflater mInflater) {
        this.mInflater = mInflater;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return this.getmData().size();
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mItemName;
        private TextView mItemId;
        private ImageView mImageIcon;
        private View itemViewS;

        public ListViewHolder(View itemView) {
            super(itemView);
            mItemName = itemView.findViewById(R.id.nombre);
            mItemId = itemView.findViewById(R.id.idHidden);
            mImageIcon = itemView.findViewById(R.id.icono);
            itemViewS = itemView;

            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            try {
                // get input stream
                InputStream ims = null;
                ims = itemViewS.getContext().getAssets().open("img/" + getmData().get(position).getIcon() + ".png");

                // load image as Drawable
                Drawable d = Drawable.createFromStream(ims, null);
                mImageIcon.setImageDrawable(d);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mItemName.setText(getmData().get(position).getName());
            mItemId.setText(getmData().get(position).getId() + "");
        }

        public void onClick(View view) {

        }

    }

}