package com.example.hejuso.wowctionhouse.App.Model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Objeto {

    private int id;
    private String name;
    private String icon;

    public Objeto() {
    }

    public Objeto(int id, String name, String icon) {
        this.name = name;
        this.id = id;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
