package com.example.hejuso.wowctionhouse.App.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hejuso.wowctionhouse.App.Model.Subastas;
import com.example.hejuso.wowctionhouse.R;

import java.util.ArrayList;

public class AdaptadorRecycler extends RecyclerView.Adapter {
    private ArrayList<Subastas> mData;
    private LayoutInflater mInflater;

    public AdaptadorRecycler(Context context, ArrayList<Subastas> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    public ArrayList<Subastas> getmData() {
        return mData;
    }

    public void setmData(ArrayList<Subastas> mData) {
        this.mData = mData;
    }

    public LayoutInflater getmInflater() {
        return mInflater;
    }

    public void setmInflater(LayoutInflater mInflater) {
        this.mInflater = mInflater;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_auction, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return this.getmData().size();
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mItemName;
        private TextView mOwner;
        private TextView mBid;
        private TextView mBuyout;
        private TextView mQuantity;

        public ListViewHolder(View itemView) {
            super(itemView);
            mItemName = itemView.findViewById(R.id.item_name);
            mOwner = itemView.findViewById(R.id.owner);
            mBid = itemView.findViewById(R.id.bid);
            mBuyout = itemView.findViewById(R.id.buyout);
            mQuantity = itemView.findViewById(R.id.quantity);

            itemView.setOnClickListener(this);
        }

        public void bindView(int position) {
            mItemName.setText(getmData().get(position).getItem());
            mOwner.setText(getmData().get(position).getOwner());
            mBid.setText(getmData().get(position).getBid());
            mQuantity.setText(getmData().get(position).getQuantity());
            mBuyout.setText(getmData().get(position).getBuyout());
        }

        public void onClick(View view) {

        }

    }

}